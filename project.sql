-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 01, 2019 at 05:16 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(10) NOT NULL,
  `admin_name` varchar(50) DEFAULT NULL,
  `admin_email` varchar(50) NOT NULL,
  `admin_password` varchar(50) NOT NULL,
  `admin_status` int(10) NOT NULL,
  `admin_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_name`, `admin_email`, `admin_password`, `admin_status`, `admin_image`) VALUES
(1, 'Admin', 'admin@gmail.com', 'admin123', 1, '../images/Profile_Images/admin.png');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `comment_id` int(10) NOT NULL,
  `comment_text` text NOT NULL,
  `comment_by` int(10) NOT NULL,
  `comment_to` int(10) NOT NULL,
  `comment_task_id` int(10) NOT NULL,
  `comment_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `comment_display` int(5) NOT NULL DEFAULT 1,
  `commenter_name` varchar(50) NOT NULL,
  `commenter_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`comment_id`, `comment_text`, `comment_by`, `comment_to`, `comment_task_id`, `comment_time`, `comment_display`, `commenter_name`, `commenter_image`) VALUES
(1, '<p>ok sir</p>', 1, 1, 1, '0000-00-00 00:00:00', 1, 'Anas', '../images/Profile_Images/jonney.jpeg1571984859'),
(2, '<p>ok</p>', 13, 13, 6, '0000-00-00 00:00:00', 1, 'Raphael', '../images/Profile_Images/1574430951'),
(3, '<p>aw</p>', 13, 12, 3, '0000-00-00 00:00:00', 1, 'Raphael', '../images/Profile_Images/greatest_bond_jesus_and_man.jpg1574436792'),
(6, '<p>aw</p>', 1, 12, 2, '0000-00-00 00:00:00', 1, 'Admin', 'hidden'),
(7, '<p>awwwwww</p>', 1, 12, 2, '0000-00-00 00:00:00', 1, 'Admin', 'hidden'),
(8, '<p>late</p>', 13, 13, 5, '0000-00-00 00:00:00', 1, 'Raphael', '../images/Profile_Images/student.png1574442056'),
(9, '<p>aw</p>', 12, 12, 14, '0000-00-00 00:00:00', 1, 'Winston', '../images/Profile_Images/student.png1574441477'),
(10, '<p>aw</p>', 13, 13, 16, '0000-00-00 00:00:00', 1, 'Raphael', '../images/Profile_Images/user.png1574749509'),
(11, '<p>hmmm</p>', 1, 13, 16, '0000-00-00 00:00:00', 1, 'Admin', '../images/Profile_Images/admin.png'),
(12, '<p>aw</p>', 13, 13, 19, '0000-00-00 00:00:00', 1, 'Raphael', '../images/Profile_Images/user.png1574749509'),
(13, '<p>aw</p>', 1, 12, 18, '0000-00-00 00:00:00', 1, 'Admin', '../images/Profile_Images/admin.png'),
(14, '<p>aw</p>', 13, 13, 21, '0000-00-00 00:00:00', 1, 'Raphael', '../images/Profile_Images/user.png1574749509'),
(15, '<p>aw</p>', 12, 12, 25, '0000-00-00 00:00:00', 1, 'Winston', '../images/Profile_Images/student.png1574441477'),
(16, '<p>ok</p>', 1, 12, 25, '0000-00-00 00:00:00', 1, 'Admin', '../images/Profile_Images/admin.png'),
(17, '<p>ok</p>', 1, 12, 25, '0000-00-00 00:00:00', 1, 'Admin', '../images/Profile_Images/admin.png'),
(18, '<p>aw</p>', 12, 12, 27, '0000-00-00 00:00:00', 1, 'Winston', '../images/Profile_Images/student.png1574441477'),
(19, '<p>awww</p>', 1, 12, 27, '0000-00-00 00:00:00', 1, 'Admin', '../images/Profile_Images/admin.png'),
(20, '<p>awww</p>', 1, 12, 27, '0000-00-00 00:00:00', 1, 'Admin', '../images/Profile_Images/admin.png'),
(21, 'aw', 13, 13, 22, '0000-00-00 00:00:00', 1, 'Raphael', '../images/Profile_Images/user.png1574749509'),
(22, 'hmmm', 12, 12, 29, '0000-00-00 00:00:00', 1, 'Winston', '../images/Profile_Images/student.png1574441477'),
(23, 'grrrr', 1, 12, 29, '2019-11-26 21:45:11', 1, 'Admin', '../images/Profile_Images/admin.png');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `project_id` int(10) NOT NULL,
  `project_name` varchar(50) NOT NULL,
  `project_description` text NOT NULL,
  `project_status` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`project_id`, `project_name`, `project_description`, `project_status`) VALUES
(1, 'Dota', '<p>Lolol</p>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `task_id` int(10) NOT NULL,
  `task_name` varchar(50) NOT NULL,
  `task_issuedate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `dead_line` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `task_details` text NOT NULL,
  `task_project` int(10) NOT NULL,
  `task_receiver` int(10) NOT NULL,
  `task_sender` int(10) NOT NULL,
  `task_sender_name` varchar(50) NOT NULL,
  `task_sender_image` text NOT NULL,
  `task_display` int(10) NOT NULL DEFAULT 1,
  `task_status` varchar(50) NOT NULL DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `task`
--

INSERT INTO `task` (`task_id`, `task_name`, `task_issuedate`, `dead_line`, `task_details`, `task_project`, `task_receiver`, `task_sender`, `task_sender_name`, `task_sender_image`, `task_display`, `task_status`) VALUES
(36, 'fff', '0000-00-00 00:00:00', '2019-12-12 00:12:00', '<p>adasdas</p>', 1, 12, 1, 'Admin', '../images/Profile_Images/admin.png', 1, 'pending'),
(37, 'dasdasd', '0000-00-00 00:00:00', '2019-12-12 02:03:00', '<p>dadasda</p>', 1, 12, 1, 'Admin', '../images/Profile_Images/admin.png', 1, 'pending'),
(38, 'dasdasda', '0000-00-00 00:00:00', '2019-12-12 00:02:00', '<p>dadasda</p>', 1, 12, 1, 'Admin', '../images/Profile_Images/admin.png', 1, 'pending'),
(39, 'ggg', '0000-00-00 00:00:00', '2019-12-12 03:02:00', '<p>dsadasda</p>', 1, 15, 1, 'Admin', '../images/Profile_Images/admin.png', 1, 'pending'),
(40, 'Go home', '2019-11-26 15:19:52', '2019-12-20 02:03:00', '<p>g</p>', 1, 12, 15, 'Mark', '../images/Profile_Images/user.png1574747493', 1, 'pending'),
(41, 'gggg', '2019-11-29 12:31:22', '2019-12-20 03:20:00', '<p>raphae</p>', 1, 13, 1, 'Admin', '../images/Profile_Images/admin.png', 1, 'completed'),
(42, 'hehe', '0000-00-00 00:00:00', '2019-12-20 03:20:00', '<p>asfsa</p>', 1, 13, 1, 'Admin', '../images/Profile_Images/admin.png', 1, 'pending'),
(43, 'fafasfas', '0000-00-00 00:00:00', '2019-12-12 03:44:00', '<p>fasfasf</p>', 1, 12, 1, 'Admin', '../images/Profile_Images/admin.png', 1, 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(10) NOT NULL,
  `user_fname` varchar(50) NOT NULL,
  `user_lname` varchar(50) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `user_password` varchar(50) NOT NULL,
  `user_status` int(10) NOT NULL,
  `user_gender` varchar(10) NOT NULL,
  `user_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_fname`, `user_lname`, `user_email`, `user_password`, `user_status`, `user_gender`, `user_image`) VALUES
(12, 'Winston', 'Lesigues', 'winston@gmail.com', '123', 1, 'male', '../images/Profile_Images/student.png1574441477'),
(13, 'Raphael', 'Abac', 'rap@gmail.com', '123', 1, 'male', '../images/Profile_Images/user.png1574749509');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`project_id`);

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`task_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `comment_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `project_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `task`
--
ALTER TABLE `task`
  MODIFY `task_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
